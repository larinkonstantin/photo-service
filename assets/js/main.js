const host = 'https://larin.dev/photo-service/api';

const f = async(url, method = 'get', data = null, useToken = true) => {
    method = method.toUpperCase();

    let options = { method, headers: {} };

    if(useToken) {
        options.headers['Authorization'] = `Bearer ${app.user.token}`;
    }

    if(data instanceof FormData) {
        options.body = data;
    } else {
        options.headers['Content-Type'] = 'application/json';

        if(['POST', 'PATCH', 'PUT'].includes(method)) {
            options.body = JSON.stringify(data);
        }
    }

    const response = await fetch(host + url, options);

    let json = null;

    try {
        json = await response.json();
    } catch (e) {}

    return {
        response,
        data: json
    };
};

let app = new Vue({
    el: '#app',
    data: {
        page: 'login',
        user: {
            token: null
        },
        forms: {
            signup: {
                fields: {
                    first_name: null,
                    surname: null,
                    phone: null,
                    password: null,
                    password_confirmation: null,
                }
            },
            login: {
                fields: {
                    phone: null,
                    password: null
                }
            }
        }
    },
    methods: {
        go(pageName) {
            this.page = pageName;
        },
        async signup() {
            await f('/signup', 'post', this.forms.signup.fields, false);
        },
        async login() {
            const { response, data } = await f('/login', 'post', this.forms.login.fields, false);

            if(response.status === 200) {
                this.user.token = data.token;
                this.go('home');
                this.saveLocalData();
            }
        },
        saveLocalData() {
            localStorage.setItem('token', this.user.token);
        },
        loadLocalData() {
            this.user.token = localStorage.getItem('token');

            if(this.user.token) {
                this.go('home');
            }
        }
    }
});

app.loadLocalData();